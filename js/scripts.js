(function ($) {
"use strict";

    // header-sticky
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 141) {
            $(".header-area").removeClass("header-sticky");
        } else {
            $(".header-area").addClass("header-sticky");
        }
    });

    // scrollIt (for header)
    $.scrollIt({
        upKey: 0, // key code to navigate to the next section
        downKey: 0, // key code to navigate to the previous section
        easing: 'linear', // the easing function for animation
        scrollTime: 900, // how long (in ms) the animation takes
        activeClass: 'active', // class given to the active nav element
        onPageChange: null, // function(pageIndex) that is called when page is changed
        topOffset: -91 // offste (in px) for fixed top navigation
    });

    // mean-menu
    $('#menu').meanmenu({
        meanMenuContainer: '.mean-menu',
        meanScreenWidth: "991",
        onePage: true,
    });

	// data-background (for background image)
	$("[data-background]").each(function() {
	    $(this).css("background-image", "url(" + $(this).attr("data-background") + ")")
	});

    // testimonial-carousel
    $('.testimonial-carousel').owlCarousel({
        loop:true,
        margin:0,
        items:1,
        autoplay:false,
        autoplayTimeout: 5000,
        navText:['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
        nav:false,
        dots:true,
    });

    // partner-carousel
    $('.partner-carousel').owlCarousel({
        loop:false,
        margin:50,
        items:5,
        autoplay: false,
        navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            767:{
                items:3
            },
            992:{
                items:5
            }
        }
    });

    // body prealoader
    $(window).on('load', function () {
        $(".body-preloader").fadeOut(500);
    });

})(jQuery);	